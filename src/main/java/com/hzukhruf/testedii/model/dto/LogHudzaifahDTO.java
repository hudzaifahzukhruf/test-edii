package com.hzukhruf.testedii.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class LogHudzaifahDTO {
	private String idRequestBooking;
	private String idPlatform;
	private String namaPlatform;
	private String docType;
	private String termOfPayment;

}
