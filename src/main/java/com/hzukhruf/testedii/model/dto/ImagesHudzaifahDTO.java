package com.hzukhruf.testedii.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ImagesHudzaifahDTO {
	private String idRequestBooking;
	private Byte description;

}
