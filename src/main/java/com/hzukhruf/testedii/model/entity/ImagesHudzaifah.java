package com.hzukhruf.testedii.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "log_hudzaifah")
public class ImagesHudzaifah {
	@Id
	@Column(name = "idrequestbooking")
	private String idRequestBooking;

	@Column(name = "description")
	private Byte description;
	

}