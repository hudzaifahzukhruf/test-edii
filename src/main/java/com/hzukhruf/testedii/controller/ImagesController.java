package com.hzukhruf.testedii.controller;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hzukhruf.testedii.model.dto.ImagesHudzaifahDTO;
import com.hzukhruf.testedii.model.entity.ImagesHudzaifah;
import com.hzukhruf.testedii.repository.ImagesRepository;
import com.hzukhruf.testedii.service.ImagesService;


@RestController
@RequestMapping("/images")
@CrossOrigin(origins = "http://localhost:3000")
public class ImagesController {
	@Autowired
	private ImagesRepository repository;
	@Autowired 
	private ImagesService service;

	@GetMapping("/")
	public ResponseEntity<List<ImagesHudzaifahDTO>> getAll() {
		List<ImagesHudzaifah> images = repository.findAll();
		List<ImagesHudzaifahDTO> imagesDtos= images.stream().map(image-> convertToDto(image))
				.collect(Collectors.toList());
		return new ResponseEntity<List<ImagesHudzaifahDTO>>(imagesDtos, HttpStatus.OK);
	}

	@GetMapping("/{id}")
	public ResponseEntity<ImagesHudzaifahDTO> get(@PathVariable String id) {
		ImagesHudzaifahDTO dto = convertToDto(repository.getOne(id));
		return new ResponseEntity<ImagesHudzaifahDTO>(dto, HttpStatus.OK);
	}

	@PutMapping("/")
	public ResponseEntity<ImagesHudzaifahDTO> update(@RequestBody ImagesHudzaifahDTO dto) {
		ImagesHudzaifah image = convertToEntity(dto);
		return new ResponseEntity<ImagesHudzaifahDTO>(convertToDto(service.update(image)), HttpStatus.OK);
	}

	@PostMapping("/")
	public ResponseEntity<ImagesHudzaifahDTO> save(@RequestBody ImagesHudzaifahDTO dto) {
		ImagesHudzaifah image= convertToEntity(dto);
		return new ResponseEntity<ImagesHudzaifahDTO>(convertToDto(service.save(image)), HttpStatus.OK);
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<ImagesHudzaifahDTO> delete(@PathVariable String id) {
		service.delete(id);
		return new ResponseEntity<ImagesHudzaifahDTO>(HttpStatus.OK);
	}

	private ImagesHudzaifah convertToEntity(ImagesHudzaifahDTO dto) {
		if (dto == null) return null;
		
		ImagesHudzaifah entity = new ImagesHudzaifah();
		
		if (dto.getIdRequestBooking() != null) {
			Optional<ImagesHudzaifah> temp = this.repository.findById(dto.getIdRequestBooking());
			if(temp.isPresent()){
				entity = temp.get();
			}
		}
		if (dto.getIdRequestBooking() != null) entity.setIdRequestBooking(dto.getIdRequestBooking());
		if(dto.getDescription()!=null) entity.setDescription(dto.getDescription());
		return entity;
	}
	
	private ImagesHudzaifahDTO convertToDto(ImagesHudzaifah entity) {
		if(entity == null) return null;
		return ImagesHudzaifahDTO.builder()
				.idRequestBooking(entity.getIdRequestBooking())
				.description(entity.getDescription())
				.build();
	}
}
