package com.hzukhruf.testedii.controller;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hzukhruf.testedii.model.dto.LogHudzaifahDTO;
import com.hzukhruf.testedii.model.entity.LogHudzaifah;
import com.hzukhruf.testedii.repository.LogRepository;
import com.hzukhruf.testedii.service.LogService;


@RestController
@RequestMapping("/log")
@CrossOrigin(origins = "http://localhost:3000")
public class LogController {
	@Autowired
	private LogRepository repository;
	@Autowired 
	private LogService service;

	@GetMapping("/")
	public ResponseEntity<List<LogHudzaifahDTO>> getAll() {
		List<LogHudzaifah> logs = repository.findAll();
		List<LogHudzaifahDTO> logDtos= logs.stream().map(log -> convertToDto(log))
				.collect(Collectors.toList());
		return new ResponseEntity<List<LogHudzaifahDTO>>(logDtos, HttpStatus.OK);
	}

	@GetMapping("/{id}")
	public ResponseEntity<LogHudzaifahDTO> get(@PathVariable String id) {
		LogHudzaifahDTO dto = convertToDto(repository.getOne(id));
		return new ResponseEntity<LogHudzaifahDTO>(dto, HttpStatus.OK);
	}

	@PutMapping("/")
	public ResponseEntity<LogHudzaifahDTO> update(@RequestBody LogHudzaifahDTO dto) {
		LogHudzaifah log = convertToEntity(dto);
		return new ResponseEntity<LogHudzaifahDTO>(convertToDto(service.update(log)), HttpStatus.OK);
	}

	@PostMapping("/")
	public ResponseEntity<LogHudzaifahDTO> save(@RequestBody LogHudzaifahDTO dto) {
		LogHudzaifah log= convertToEntity(dto);
		return new ResponseEntity<LogHudzaifahDTO>(convertToDto(service.save(log)), HttpStatus.OK);
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<LogHudzaifahDTO> delete(@PathVariable String id) {
		service.delete(id);
		return new ResponseEntity<LogHudzaifahDTO>(HttpStatus.OK);
	}

	private LogHudzaifah convertToEntity(LogHudzaifahDTO dto) {
		if (dto == null) return null;
		
		LogHudzaifah entity = new LogHudzaifah();
		
		if (dto.getIdRequestBooking() != null) {
			Optional<LogHudzaifah> temp = this.repository.findById(dto.getIdRequestBooking());
			if(temp.isPresent()){
				entity = temp.get();
			}
		}
		if (dto.getIdRequestBooking() != null) entity.setIdRequestBooking(dto.getIdRequestBooking());
		if (dto.getIdPlatform() != null) entity.setIdPlatform(dto.getIdPlatform());
		if (dto.getNamaPlatform() != null) entity.setNamaPlatform(dto.getNamaPlatform());
		if(dto.getDocType()!=null) entity.setDocType(dto.getDocType());
		if(dto.getTermOfPayment()!=null) entity.setTermOfPayment(dto.getTermOfPayment());
		return entity;
	}
	
	private LogHudzaifahDTO convertToDto(LogHudzaifah entity) {
		if(entity == null) return null;
		return LogHudzaifahDTO.builder()
				.idRequestBooking(entity.getIdRequestBooking())
				.idPlatform(entity.getIdPlatform())
				.namaPlatform(entity.getNamaPlatform())
				.docType(entity.getDocType())
				.termOfPayment(entity.getTermOfPayment())
				.build();
	}
}
