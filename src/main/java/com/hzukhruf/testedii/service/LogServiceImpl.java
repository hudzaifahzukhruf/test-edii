package com.hzukhruf.testedii.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hzukhruf.testedii.model.entity.LogHudzaifah;
import com.hzukhruf.testedii.repository.LogRepository;


@Service
@Transactional
public class LogServiceImpl implements LogService{
    @Autowired
    private LogRepository repository;

	@Override
	public LogHudzaifah save(LogHudzaifah entity) {
		return repository.save(entity);
	}

	@Override
	public LogHudzaifah update(LogHudzaifah entity) {
		return repository.save(entity);
	}

	@Override
	public LogHudzaifah delete(String id) {
		LogHudzaifah log = repository.findById(id).get();
		repository.deleteById(id);
		return log;
	}

}
