package com.hzukhruf.testedii.service;

import com.hzukhruf.testedii.model.entity.LogHudzaifah;

public interface LogService {
    LogHudzaifah save(LogHudzaifah entity);
    LogHudzaifah update(LogHudzaifah entity);
    LogHudzaifah delete(String id);
}
