package com.hzukhruf.testedii.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hzukhruf.testedii.model.entity.ImagesHudzaifah;
import com.hzukhruf.testedii.repository.ImagesRepository;


@Service
@Transactional
public class ImagesServiceImpl implements ImagesService{
    @Autowired
    private ImagesRepository repository;

	@Override
	public ImagesHudzaifah save(ImagesHudzaifah entity) {
		return repository.save(entity);
	}

	@Override
	public ImagesHudzaifah update(ImagesHudzaifah entity) {
		return repository.save(entity);
	}

	@Override
	public ImagesHudzaifah delete(String id) {
		ImagesHudzaifah images = repository.findById(id).get();
		repository.deleteById(id);
		return images;
	}

}
