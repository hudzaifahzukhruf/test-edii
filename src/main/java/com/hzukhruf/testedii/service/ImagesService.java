package com.hzukhruf.testedii.service;

import com.hzukhruf.testedii.model.entity.ImagesHudzaifah;

public interface ImagesService {
    ImagesHudzaifah save(ImagesHudzaifah entity);
    ImagesHudzaifah update(ImagesHudzaifah entity);
    ImagesHudzaifah delete(String id);
}
