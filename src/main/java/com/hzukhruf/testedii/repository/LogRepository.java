package com.hzukhruf.testedii.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.hzukhruf.testedii.model.entity.LogHudzaifah;


@Repository
public interface LogRepository extends JpaRepository<LogHudzaifah, String> {

}
