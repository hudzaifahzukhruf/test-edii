$(document).ready(function () {
	//console.log('test');
	table.create();
});

$("#save").click(function () {
	//console.log('testttt');

	form.saveForm()
	form.resetForm();

})

var form = {
	resetForm: function () {
		$('#form')[0].reset();
	},
	saveForm: function () {
		var dataResult = getJsonForm($("#form").serializeArray(), true);
		console.log(dataResult)
		$.ajax({
			url: '/product/',
			method: 'post',
			contentType: 'application/json',
			dataType: 'json',
			data: JSON.stringify(dataResult),
			success: function (result, status, xhr) {
				//				console.log(result)
				//				console.log(status)
				//				console.log(xhr)
				if (xhr.status == 200 || xhr.status == 201) {
					//alert("sukses")
					table.create();
				} else {
					alert("error")
				}
			},
			error: function (err) {
				console.log(err);
			}
		});
	},
}
var table = {
	create: function () {
		$.ajax({
			url: '/product/',
			method: 'get',
			contentType: 'application/json',
			success: function (res, status, xhr) {
				if (xhr.status == 200 || xhr.status == 201) {
					$('#tbodyProduct').html("");
					$.each(res, function (index, item) {
						$('#tbodyProduct').append(
							"<tr>" +
							"<td style='text-align:center;width:30px'>" + item.code + "</td>" +
							"<td style='width:70px'>" + item.product + "</td>" +
							"<td style='text-align:right;width:40px'>" + item.price + "</td>" +
							"</tr>"
						);
					});
				}
			},
			error: function (err) {
				console.log(err);
			}
		});
	}
}